Usage :
=======
Run the container with the following command and, if compiling for blue, change `BOLOS_SDK` for `$BLUE_S_SDK_DIR`
```shell
# standard usage
docker run --rm -it --net=none --privileged -v $(pwd):/DATA registry.gitlab.com/thomasblt/bolos-dev
# sortcut setup for nano-s with BOLOS_SDK="$NANO_S_SDK_DIR"
docker run --rm -it --net=none --privileged -v $(pwd):/DATA registry.gitlab.com/thomasblt/bolos-dev/nanos
# sortcut setup for blue with BOLOS_SDK="$BLUE_S_SDK_DIR"
docker run --rm -it --net=none --privileged -v $(pwd):/DATA registry.gitlab.com/thomasblt/bolos-dev/blue
```
NB. `--privileged` is used to give full access to the host's USB bus (and there might be a better solution) to upload and remove the app.
It's useless for compilation purposes.

WARNING 1/2:
============
This container may work properly when you try to interact with your physical device !

See [ledger's python loader](https://github.com/LedgerHQ/blue-loader-python) warning :

> It is recommended to install [...] in your native environment (**not a Docker image**) to avoid hidapi issues

WARNING 2/2:
============
If you didn't care about the first warning please be prepared for some rodeo !

Each time you plug the device / upload an app / use an app / remove an app you have to restart your container.

If you use Virtualbox, you have to attach the USB device to your box before starting/restarting the container.

Example usage :
=========
```shell
### On the host
# Download sample app
wget https://github.com/LedgerHQ/blue-sample-apps/archive/master.zip
#unzip source
unzip master.zip
# get in source dir
cd blue-sample-apps-master/
# launch the container
docker run --rm -it --privileged -v $(pwd):/DATA registry.gitlab.com/thomasblt/bolos-dev
### inside container
# Let's compile the helloworld app
cd blue-app-helloworld
# let's compile the app
make
# let's load the app to the device
make load
# play with the app ... and then remove it
make delete
```